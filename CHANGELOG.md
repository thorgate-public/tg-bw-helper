# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2022-02-10

### Changed

* Improved documentation

## [1.0.1] - 2021-08-14

### Added

* CI packaging for pypi

## [1.0.0] - 2021-08-14

### Added

* Password helper
* Tests

[Unreleased]: https://gitlab.com/thorgate-public/tg-bw-helper/-/compare/v1.0.2...master
[1.0.2]: https://gitlab.com/thorgate-public/tg-bw-helper/-/compare/v1.0.2...v1.0.1
[1.0.1]: https://gitlab.com/thorgate-public/tg-bw-helper/-/compare/v1.0.1...v1.0.0
[1.0.0]: https://gitlab.com/thorgate-public/tg-bw-helper/-/tags/v1.0.0
