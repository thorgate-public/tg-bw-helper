CYAN ?= \033[0;36m
COFF ?= \033[0m

.PHONY:
all: help

.PHONY:
help:
	@echo -e "+------<<<<                                     Tasks                                    >>>>------+"
	@echo -e "$(CYAN)make test$(COFF)               - Runs automatic tests on your python code"
	@echo -e "$(CYAN)make setup$(COFF)              - Prepares virtual environment for development"
	@echo -e "$(CYAN)make coverage$(COFF)           - Produces coverage report"
	@echo -e "$(CYAN)make quality$(COFF)            - Checks code quality"
	@echo -e "$(CYAN)make black-check-all$(COFF)    - Checks code formatting with black"
	@echo -e "$(CYAN)make black-format-all$(COFF)   - Reformats code with black"
	@echo -e "$(CYAN)make isort$(COFF)              - Checks import order with isort"
	@echo -e "$(CYAN)make isort-fix$(COFF)          - Sorts import order"
	@echo -e "$(CYAN)make prospector$(COFF)         - Analyzes code for problems with prospector"

.PHONY:
setup:
	poetry env use python3.9
	poetry install

.PHONY:
test:
	PYTHONPATH=$(CURDIR) poetry run pytest

.PHONY:
coverage:
	PYTHONPATH=$(CURDIR) poetry run coverage run -m pytest
	poetry run coverage report -m

.PHONY:
quality: black-check-all prospector isort

.PHONY:
black-check-all:
	poetry run black --check ./tg_bw_helper

.PHONY:
black-format-all:
	poetry run black ./tg_bw_helper

.PHONY:
isort:
	poetry run isort --recursive --check-only --diff ./tg_bw_helper

.PHONY:
isort-fix:
	poetry run isort --recursive ./tg_bw_helper

.PHONY:
prospector:
	poetry run prospector --no-autodetect tg_bw_helper

.PHONY:
package:
	poetry build

.PHONY:
shell:
	poetry run python


.PHONY:
readme: # Render README preview locally with the package that PyPI using
	poetry run python -m readme_renderer README.rst -o /tmp/README.html
